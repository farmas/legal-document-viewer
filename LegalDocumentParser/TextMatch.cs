﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalDocumentViewer
{
    public class TextMatch
    {
        private readonly string _text;
        private readonly int _startIndex;

        public TextMatch(string text, int startIndex)
        {
            _text = text;
            _startIndex = startIndex;
            SubMatches = new List<TextMatch>();

        }

        public IList<TextMatch> SubMatches { get; private set; }

        public string Text
        {
            get
            {
                return _text;
            }
        }

        public int StartIndex
        {
            get
            {
                return _startIndex;
            }
        }

        public int EndIndex
        {
            get
            {
                return _startIndex + _text.Length;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace LegalDocumentViewer
{
    public class TextMatchViewModel : ViewModelBase
    {
        private readonly TextMatch _match;
        private readonly IList<SubTextMatchViewModel> _subMatches;

        private bool _isSelected;
        private bool _isExpanded;

        public TextMatchViewModel(TextMatch match)
        {
            _match = match;
            _subMatches = new List<SubTextMatchViewModel>(match.SubMatches.Select(s => new SubTextMatchViewModel(s)));
        }

        public IList<SubTextMatchViewModel> SubMatches
        {
            get
            {
                return _subMatches;
            }
        }

        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public string Text
        {
            get
            {
                return _match.Text;
            }
        }

        public int StartIndex
        {
            get
            {
                return _match.StartIndex;
            }
        }

        public int EndIndex
        {
            get
            {
                return _match.EndIndex;
            }
        }
    }
}
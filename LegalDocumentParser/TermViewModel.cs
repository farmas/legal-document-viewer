﻿using System.Collections.Generic;
using System.Linq;

namespace LegalDocumentViewer
{
    public class TermViewModel : ViewModelBase
    {
        private readonly TextMatch _match;
        private readonly IList<SubTermViewModel> _subTerms;

        private bool _isSelected;
        private bool _isExpanded;

        public TermViewModel(TextMatch match)
        {
            _match = match;
            _subTerms = new List<SubTermViewModel>(match.SubMatches.Select(s => new SubTermViewModel(s)));
        }

        public IList<SubTermViewModel> SubTerms
        {
            get
            {
                return _subTerms;
            }
        }

        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public string Text
        {
            get
            {
                return _match.Text;
            }
        }

        public int StartIndex
        {
            get
            {
                return _match.StartIndex;
            }
        }

        public int EndIndex
        {
            get
            {
                return _match.EndIndex;
            }
        }
    }
}
﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalDocumentViewer
{
    public class WindowServices : IWindowServices
    {
        public string OpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set filter and RestoreDirectory
            openFileDialog.RestoreDirectory = true;
            openFileDialog.Filter = "Word documents(*.doc;*.docx)|*.doc;*.docx";

            bool? result = openFileDialog.ShowDialog();
            if (result == true && openFileDialog.FileName.Length > 0)
            {
                return openFileDialog.FileName;
            }
            else
            {
                return null;
            }
        }

        public string SaveFileDialog(string title)
        {
            return SaveFileDialog(title, "Word documents(*.docx)|*.docx", null);
        }

        public string SaveFileDialog(string title, string filter, string defaultFileName)
        {
            var dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.RestoreDirectory = true;
            dialog.Filter = filter;
            dialog.Title = title;
            dialog.FileName = defaultFileName;

            bool? result = dialog.ShowDialog();
            if (result == true && dialog.FileName.Length > 0)
            {
                return dialog.FileName;
            }
            else
            {
                return null;
            }
        }
    }
}

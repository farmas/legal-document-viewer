﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LegalDocumentViewer
{
    public class DocumentLoadedEventArgs : EventArgs
    {
        public IList<string> Paragraphs { get; set; }
    }

    public class SelectedTermChangedEventArgs : EventArgs
    {
        public TextMatchViewModel NewSelectedTerm { get; set; }
        public TextMatchViewModel OldSelectedTerm { get; set; }
    }

    public class DocumentViewerViewModel : ViewModelBase
    {
        private const string STATUS_READY = "Ready.";
        private const string FILTER_ALL = "Show Defined Terms";
        private const string FILTER_EMPTY = "Show Unused Terms";
        private const string FILTER_DUPLICATED = "Show Duplicated Terms";
        private const string FILTER_UNDEFINED = "Show Undefined Terms";

        private readonly IWindowServices _windowServices;
        private TextMatchViewModel _selectedTerm;
        private string _selectedFileName;
        private string _selectedFilter = FILTER_ALL;
        private string _status = STATUS_READY;

        public event EventHandler DocumentLoading;
        public event EventHandler<DocumentLoadedEventArgs> DocumentLoaded;
        public event EventHandler<SelectedTermChangedEventArgs> SelectedTermChanged;

        public DelegateCommand LoadFileCommand { get; private set; }
        public DelegateCommand SaveFileCommand { get; private set; }
        public DelegateCommand ExportListCommand { get; private set; }

        public ObservableCollection<TextMatchViewModel> FilteredTerms { get; private set; }
        public ObservableCollection<TextMatchViewModel> Terms { get; private set; }
        public ObservableCollection<TextMatchViewModel> UndefinedTerms { get; private set; }

        public DocumentViewerViewModel(IWindowServices windowServices)
        {
            _windowServices = windowServices;
            FilteredTerms = new ObservableCollection<TextMatchViewModel>();
            Terms = new ObservableCollection<TextMatchViewModel>();
            UndefinedTerms = new ObservableCollection<TextMatchViewModel>();

            this.LoadFileCommand = new DelegateCommand(async () => await LoadFile());
            this.SaveFileCommand = new DelegateCommand(async () => await SaveFile());
            this.ExportListCommand = new DelegateCommand(() => ExportList());
        }

        public string SelectedFileName
        {
            get
            {
                return _selectedFileName;
            }
            set
            {
                _selectedFileName = value;
                OnPropertyChanged(nameof(SelectedFileName));
            }
        }

        public string[] Filters
        {
            get
            {
                return new string[] { FILTER_ALL, FILTER_EMPTY, FILTER_DUPLICATED };
            }
        }

        public TextMatchViewModel SelectedTerm
        {
            get
            {
                return _selectedTerm;
            }

            set
            {
                if (_selectedTerm != null)
                {
                    _selectedTerm.IsSelected = false;
                }

                if (value != null)
                {
                    value.IsSelected = true;
                }

                var termChangedArgs = new SelectedTermChangedEventArgs()
                {
                    NewSelectedTerm = value,
                    OldSelectedTerm = _selectedTerm
                };

                _selectedTerm = value;
                OnPropertyChanged(nameof(SelectedTerm));
                OnSelectedTermChanged(termChangedArgs);
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        public string SelectedFilter
        {
            get
            {
                return _selectedFilter;
            }

            set
            {
                _selectedFilter = value;
                OnPropertyChanged(nameof(SelectedFilter));

                FilterTerms();
            }
        }

        private void ExportList()
        {
            var newFileName = _windowServices.SaveFileDialog("Export List to File", "HTML Files(*.html)|*.html", "Legal Document List.html");

            if (!String.IsNullOrEmpty(newFileName))
            {
                var textMatches = from match in GetTextMatchesForSelection()
                                  group match by match.Text into matchGroup
                                  select new
                                  {
                                      Text = matchGroup.Key,
                                      Uses = matchGroup.First().SubMatches.Count(),
                                      Duplicates = matchGroup.Count()
                                  };

                var listItems = textMatches.Select(match => $@"
<li>
   -&nbsp;<span class=""term"">{WebUtility.HtmlEncode(match.Text)}</span>
   <span class=""duplicates duplicate-count-{match.Duplicates}"">({match.Duplicates} duplicates)</span>
   <span class=""uses use-count-{match.Uses}"">({match.Uses} uses)</span>
</li>");

                var fileContents = @"
<html>
  <head>
    <style>
       ul {
         -webkit-column-count: 3; /* Chrome, Safari, Opera */
         -moz-column-count: 3; /* Firefox */
         column-count: 3;
        list-style: none;
      }

      .uses {
          color: blue;
      }

      .duplicates,
      .use-count-0 {
          color: red;
      }

      .duplicate-count-1 {
          display: none;
      }
    </style>
  </head>
  <body>
    <ul>" + String.Join(Environment.NewLine, listItems) + @"</ul>
  </body>
</html>";

                File.WriteAllText(newFileName, fileContents);

                Process.Start(newFileName);
            }
        }

        private async Task SaveFile()
        {
            var newFileName = _windowServices.SaveFileDialog("Save As New File With Highlighted Terms");

            if (!String.IsNullOrEmpty(newFileName) && !String.IsNullOrEmpty(this.SelectedFileName))
            {
                Status = "Saving new file with highlights (this may take several minutes).";

                var document = await LegalDocumentParser.LoadDocumentAsync(this.SelectedFileName);
                var terms = this.Terms
                    .Where(term => term.SubMatches.Count > 0 && term.SubMatches.Count <= 10)
                    .Select(term => term.Text)
                    .Distinct();

                using (document)
                {
                    await LegalDocumentParser.HighlightDocument(document, terms);
                    await Task.Run(() => document.SaveAs(newFileName));
                    Status = STATUS_READY;
                }
            }
        }

        public async Task LoadFile()
        {
            var fileName = _windowServices.OpenFileDialog();

            if (!String.IsNullOrEmpty(fileName))
            {
                Status = "Loading File.";
                SelectedFileName = fileName;
                OnDocumentLoading();
                Terms.Clear();
                FilteredTerms.Clear();
                UndefinedTerms.Clear();
                SelectedFilter = FILTER_ALL;

                var document = await LegalDocumentParser.LoadDocumentAsync(fileName);

                using (document)
                {
                    var paragraphs = document.Paragraphs.Select(p => p.Text).ToList();
                    var text = String.Join("\n", paragraphs);

                    var documentLoadedArgs = new DocumentLoadedEventArgs() { Paragraphs = paragraphs };
                    OnDocumentLoaded(documentLoadedArgs);

                    Status = "Parsing Defined Terms (this may take a couple of minutes).";

                    var definedMatches = await LegalDocumentParser.GetDefinedTermsAsync(text);
                    var terms = definedMatches.Select(m => new TextMatchViewModel(m)).OrderBy(t => t.Text);

                    foreach (var term in terms)
                    {
                        Terms.Add(term);
                    }

                    FilterTerms();

                    Status = "Parsing Undefined Terms (this may take a couple of minutes).";

                    var undefinedMatches = await LegalDocumentParser.GetUndefinedTermsAsync(text, definedMatches);
                    var undefinedTerms = undefinedMatches.Select(m => new TextMatchViewModel(m)).OrderBy(t => t.Text);

                    foreach (var term in undefinedTerms)
                    {
                        UndefinedTerms.Add(term);
                    }

                    Status = STATUS_READY;
                }
            }
        }

        private void FilterTerms()
        {
            FilteredTerms.Clear();
            SelectedTerm = null;

            var filteredTerms = GetTextMatchesForSelection();

            foreach (var term in filteredTerms)
            {
                this.FilteredTerms.Add(term);
            }
        }

        private IEnumerable<TextMatchViewModel> GetTextMatchesForSelection()
        {
            IEnumerable<TextMatchViewModel> filteredTerms = Terms;

            if (SelectedFilter == FILTER_EMPTY)
            {
                filteredTerms = filteredTerms.Where(term => term.SubMatches.Count == 0);
            }
            else if (SelectedFilter == FILTER_DUPLICATED)
            {
                filteredTerms = filteredTerms
                    .GroupBy(term => term.Text)
                    .Where(group => group.Count() > 1)
                    .SelectMany(group => group);
            }
            else if (SelectedFilter == FILTER_UNDEFINED)
            {
                filteredTerms = UndefinedTerms;
            }

            return filteredTerms;
        }

        private void OnDocumentLoaded(DocumentLoadedEventArgs args)
        {
            var handler = this.DocumentLoaded;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private void OnDocumentLoading()
        {
            var handler = this.DocumentLoading;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        private void OnSelectedTermChanged(SelectedTermChangedEventArgs args)
        {
            var handler = this.SelectedTermChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}

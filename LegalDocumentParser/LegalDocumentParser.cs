﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LegalDocumentViewer
{
    public static class LegalDocumentParser
    {
        private static Dictionary<string, string> _articlesMap;

        public static Task<Novacode.DocX> LoadDocumentAsync(string filePath)
        {
            return Task.Run(() =>
            {
                var paragraphs = new List<string>();
                var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (var streamReader = new StreamReader(fs))
                {
                    return Novacode.DocX.Load(streamReader.BaseStream);
                }
            });
        }

        public static Task HighlightDocument(Novacode.DocX document, IEnumerable<string> terms)
        {
            return Task.Run(() =>
            {
                var formatting = new Novacode.Formatting();
                formatting.Highlight = Novacode.Highlight.green;

                foreach (var term in terms)
                {
                    var cleanTermPattern = GetPatternFromTerm(term);
                    document.ReplaceText(cleanTermPattern, (oldValue) => oldValue, false, RegexOptions.Multiline, formatting);
                }
            });
        }

        public static Task<IEnumerable<TextMatch>> GetUndefinedTermsAsync(string text, IEnumerable<TextMatch> definedTerms)
        {
            return Task.Run(() =>
            {
                if (_articlesMap == null)
                {
                    _articlesMap = File.ReadAllLines("Dictionary-Articles.txt").ToDictionary(line => line, StringComparer.OrdinalIgnoreCase);
                }

                var termsMap = definedTerms.Select(term => term.Text.Trim('"', '“', '”')).Distinct().ToDictionary(term => term);
                var results = new Dictionary<string, TextMatch>();
                var pattern = "[A-Z][\\w-]{2,}( +[a-zA-Z][\\w-]*){0,5} +[A-Z][\\w-]*";
                var matches = FindAllByPattern(text, pattern, RegexOptions.Singleline);

                foreach (var match in matches)
                {
                    var cleanMatch = TrimStartArticle(match);
                    var isValidTerm = IsValidTerm(cleanMatch.Text);
                    var isTermDefined = termsMap.ContainsKey(cleanMatch.Text);

                    if (isValidTerm && !isTermDefined)
                    {
                        TextMatch rootMatch;
                        if (!results.TryGetValue(cleanMatch.Text, out rootMatch))
                        {
                            results.Add(cleanMatch.Text, cleanMatch);
                        }
                        else
                        {
                            if (!rootMatch.SubMatches.Any())
                            {
                                rootMatch.SubMatches.Add(new TextMatch(rootMatch.Text, rootMatch.StartIndex));
                            }

                            rootMatch.SubMatches.Add(cleanMatch);
                        }
                    }
                }

                return (IEnumerable<TextMatch>)results.Values;
            });
        }

        private static bool IsValidTerm(string term)
        {
            var isValid = false;
            var words = term.Split(' ');

            if (!String.IsNullOrWhiteSpace(term) && words.Count() > 1)
            {
                var firstWord = words.First();

                // first word has to be capitalized, all other words have to be capitalized or be an article.
                if (IsFirstLetterCapitalized(firstWord))
                {
                    isValid = words.Skip(1).All(word => word.Length > 1 && (IsFirstLetterCapitalized(word) || IsArticle(word)));
                }
            }

            return isValid;
        }

        private static bool IsArticle(string word)
        {
            return _articlesMap.ContainsKey(word);
        }

        private static bool IsFirstLetterCapitalized(string word)
        {
            return Regex.IsMatch(word, "^[A-Z]");
        }

        private static TextMatch TrimArticles(TextMatch match)
        {
            var startTrim = TrimStartArticle(match);
            return TrimEndArticle(startTrim);
        }

        private static TextMatch TrimStartArticle(TextMatch match)
        {
            if (!String.IsNullOrWhiteSpace(match.Text))
            {
                var words = match.Text.Split(' ');
                var firstWord = words.First();

                if (_articlesMap.ContainsKey(firstWord))
                {
                    match = new TextMatch(String.Join(" ", words.Skip(1)), match.StartIndex + firstWord.Length + 1);
                }
            }

            return match;
        }

        private static TextMatch TrimEndArticle(TextMatch match)
        {
            if (!String.IsNullOrWhiteSpace(match.Text))
            {
                var words = match.Text.Split(' ');
                var lastWord = words.Last();

                if (_articlesMap.ContainsKey(lastWord))
                {
                    var wordCount = words.Count();
                    match = new TextMatch(String.Join(" ", words.Take(wordCount - 1)), match.StartIndex);
                }
            }

            return match;
        }

        public static Task<IEnumerable<TextMatch>> GetDefinedTermsAsync(string text)
        {
            return Task.Run(() =>
            {
                var matchMap = new ConcurrentDictionary<string, IEnumerable<TextMatch>>();
                var results = new ConcurrentBag<TextMatch>();
                var matches = FindAllByPattern(text, "[\"“”].+?[\"“”]");

                Parallel.ForEach(matches, (match) =>
                {
                    IEnumerable<TextMatch> subMatches;
                    results.Add(match);

                    if (!matchMap.TryGetValue(match.Text, out subMatches))
                    {
                        // Find all uses of the term.
                        var cleanTermPattern = GetPatternFromTerm(match.Text);
                        subMatches = FindAllByPattern(text, cleanTermPattern);
                        matchMap.TryAdd(match.Text, subMatches);
                    }

                    foreach (var subMatch in subMatches)
                    {
                        match.SubMatches.Add(subMatch);
                    }
                });

                return (IEnumerable<TextMatch>)results.ToArray();
            });
        }

        private static string GetPatternFromTerm(string term)
        {
            var cleanTerm = term.Trim('"', '“', '”');
            return $"([^a-zA-Z0-9\"“”]{cleanTerm}[^a-zA-Z0-9\"“”])";
        }

        private static IEnumerable<TextMatch> FindAllByPattern(string text, string pattern, RegexOptions options = RegexOptions.Multiline)
        {
            return Regex.Matches(text, pattern, options)
                .Cast<Match>()
                .Select(m => new TextMatch(m.Value, m.Index));
        }
    }
}

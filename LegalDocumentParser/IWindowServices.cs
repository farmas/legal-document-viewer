﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LegalDocumentViewer
{
    public interface IWindowServices
    {
        string OpenFileDialog();
        string SaveFileDialog(string title);
        string SaveFileDialog(string title, string filter, string defaultFileName);
    }
}

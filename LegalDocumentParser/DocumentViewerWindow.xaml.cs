﻿using System;
using System.Deployment.Application;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;

namespace LegalDocumentViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class DocumentViewerWindow : Window
    {
        private readonly DocumentViewerViewModel _viewModel;
        private TextRange _textRange;

        public DocumentViewerWindow(DocumentViewerViewModel viewModel)
        {
            this.DataContext = _viewModel = viewModel;
            InitializeComponent();

            this.Title = this.Title + " - v" + GetPublishedVersionString();
            _textRange = new TextRange(LegalFlowDocument.ContentStart, LegalFlowDocument.ContentEnd);
            viewModel.DocumentLoading += ViewModel_DocumentLoading;
            viewModel.DocumentLoaded += ViewModel_DocumentLoaded;
            viewModel.SelectedTermChanged += ViewModel_SelectedTermChanged;
        }

        private static string GetPublishedVersionString()
        {
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                return ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            else
            {
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + " (local)";
            }
        }

        private void ViewModel_DocumentLoading(object sender, EventArgs e)
        {
            LegalFlowDocument.Blocks.Clear();
        }

        private void ViewModel_DocumentLoaded(object sender, DocumentLoadedEventArgs e)
        {
            foreach (var paragraph in e.Paragraphs)
            {
                LegalFlowDocument.Blocks.Add(new Paragraph(new Run(paragraph)));
            }
        }

        private async void ViewModel_SelectedTermChanged(object sender, SelectedTermChangedEventArgs e)
        {
            await Task.Run(() => Thread.Sleep(100));
            _textRange.ClearAllProperties();

            if (e.NewSelectedTerm != null)
            {
                HighlightTerm(e.NewSelectedTerm);
            }
        }

        private void TermsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var term = e.NewValue as TextMatchViewModel;
            _viewModel.SelectedTerm = term;
        }

        private void HighlightTerm(TextMatchViewModel term)
        {
            var startingPos = GetPointerFromCharOffset(term.StartIndex + 1);
            var endingPos = GetPointerFromCharOffset(term.EndIndex + 1);

            var element = startingPos.Parent as FrameworkContentElement;

            if (element != null)
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate
                {
                    element.BringIntoView();

                    _textRange.Select(startingPos, endingPos);
                    _textRange.ApplyPropertyValue(TextElement.BackgroundProperty, Brushes.Yellow);
                }));
            }
        }

        public TextPointer GetPointerFromCharOffset(int charOffset)
        {
            int counter = 0;
            TextPointer result = LegalFlowDocument.ContentStart;
            while ((result != null) && (counter < charOffset))
            {
                if (result.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
                    String textRun = result.GetTextInRun(LogicalDirection.Forward);

                    if (counter + textRun.Length < charOffset)
                    {
                        counter += textRun.Length;
                        result = result.GetPositionAtOffset(textRun.Length);
                    }
                    else
                    {
                        var remaining = charOffset - counter;
                        result = result.GetPositionAtOffset(remaining);
                        counter += textRun.Length;
                    }
                }
                else
                {
                    result = result.GetNextInsertionPosition(LogicalDirection.Forward);
                    counter++;
                }
            }

            return result == null ? LegalFlowDocument.ContentEnd : result;
        }
    }
}
